## Proposal for new ProductConfigurationFile format:

This proposal includes:

* An example of a new ProductConfigurationFile format (.yml)

* A software tool (pcf_tool.py) for transformation of the new ProductConfigurationFile.yml into any other text file format.
(This tool ensures retro-compatibility with the old-fashioned fortran NAMELISTs)

* Two examples of ProductConfigurationFile templates.

The pcf_tool.py generates a customized ProductConfiguration file based on the default one, which is a yaml file.
The customized ProductConfigurationFile will be written by rendering a template file that must be written according with the jinja template rules.

Checkout detailed documentation about jinja templates at:
http://jinja.pocoo.org/docs/2.10/templates/

* Run the following commands with the provided template examples and try to build your own ones, to see how it works:

```sh
./pcf_tool.py examples/ProductConfigurationFile.yml examples/ProductConfigurationFile_template_old-fachioned-namelist test_out1.yml 
```

```sh
./pcf_tool.py examples/ProductConfigurationFile.yml examples/ProductConfigurationFile_template_other-namelist test_out2.yml
```

* Run ```./pcf_tool.py --help``` to display usage information.

* Install dependencies:

`pip install pyyaml jinja2`
