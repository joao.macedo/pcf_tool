#!/usr/bin/env python
#-*.coding:utf8-*-
"""   The pcf_tool generates a customized ProductConfiguration 
   file based on the default one, which is written as a yaml file.
   The customized ProductConfigurationFile will be written by
   rendering a template file that must be written according with
   the jinja template rules.

   Checkout detailed documentation about jinja templates at:
   http://jinja.pocoo.org/docs/2.10/templates/

"""
import jinja2
import yaml
import argparse

def _load_pcf_context(yaml_path):
    with open(yaml_path, 'r') as yaml_fid:
        context = yaml.load(yaml_fid)
    return context

def _load_pcf_tmplate(template_path):
    with open(template_path, 'r') as template_fid:
        template_str_list = template_fid.readlines()
    return ''.join(template_str_list)

def _render_pcf_context(template_str, context):
    env = jinja2.Environment()
    template = env.from_string(template_str)
    return template.render(context)
 
def write_customized_pcf(yaml_path, template_path,
                         customized_path):
    context = _load_pcf_context(yaml_path)
    template_str = _load_pcf_tmplate(template_path)
    customized_pcf_str = _render_pcf_context(template_str, context)
    with open(customized_path, 'w') as customized_pcf_fid:
        customized_pcf_fid.write(customized_pcf_str)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__
    )
    parser.add_argument("yaml_path",
                        help="Path to default (yaml) "
                        "ProductConfigurationFile.")
    parser.add_argument("template_path",
                         help="Path to ProductConfigurationFile template, "
                        "to be rendered.")
    parser.add_argument("customized_path", help="Path to customized "
                        "ProductConfigurationFile")
    args = parser.parse_args()
    write_customized_pcf(**vars(args))
